# معلومات تجديد الجواز رقمياً

## هوية المتقدم بالطلب

### الاسم الكامل بالعربية
عروة مؤمن مأمون ديرانية

### الاسم الكامل بالإنكليزية
Orwa Mu'men Ma'moun Diraneyya

## المعلومات الشخصية للتواصل

### العنوان البريدي في ألمانيا
Martinstraße 1, 52062 Aachen  
NRW (North Rhine-Westphalia), Germany

### الرقم الهاتفي في ألمانيا
+49 1514 3161 634

### عنوان البريد الإلكتروني
[orwa.diraneyya@gmail.com](mailto:orwa.diraneyya@gmail.com)

### رقم الواتس أب
![](whatsapp.jpeg)

## الصورة الشخصية للجواز بصورة رقمية
اضغط بالزر الأيمن للماوس واختر حفظ باسم لحفظ الصورة رقمياً  
_Right click and choose "save image as" to store image digitally_  
![](passport-pic.jpeg)

